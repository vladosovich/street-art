const express = require("express");
const router = express.Router();
const faker = require("faker");

router.get("/arts", (req, res) => {
  var arts = [
    { name: "Лучшие математики", id: "jaba" },
    { name: "Вкусная пицца", id: "circle" },
    { name: "Остров", id: "optimus" },
    { name: "Хамон", id: "bestshows" }
  ];

  res.render("arts", { arts: arts });
});

router.get("/street", (req, res) => {
  res.render("street", {
    street: [
      {
        name: "Лучшие математики",
        id: "jABA",
        url:
          "https://d32dm0rphc51dk.cloudfront.net/_80uhZu_hHGsO6m443w_hg/larger.jpg"
      },
      {
        name: "Вкусная пицца",
        id: "CIRCLE",
        url:
          "https://d32dm0rphc51dk.cloudfront.net/2M7SosrRA1bkWYGqyUbFxQ/larger.jpg"
      },
      {
        name: "Остров",
        AirInn: "OPTIMUS",
        url:
          "https://d32dm0rphc51dk.cloudfront.net/sD6AqWVeBuNy-jya6S1WbA/larger.jpg"
      },
      {
        name: "Хамон",
        AirInn: "bestshows",
        url:
          "https://d32dm0rphc51dk.cloudfront.net/Vv3UW1uGgDwFNYxLvuVXEQ/larger.jpg"
      }
    ]
  });
});
module.exports = router;

router.get("/authors", (req, res) => {
  res.render("authors", {
    authors: [
      {
        name: "Баксия Жан-мишель",
        id: "безыменная картина",
        url:
          "https://upload.wikimedia.org/wikipedia/ru/f/ff/%D0%96%D0%B0%D0%BD-%D0%9C%D0%B8%D1%88%D0%B5%D0%BB%D1%8C_%D0%91%D0%B0%D1%81%D0%BA%D0%B8%D1%8F_%D0%9F%D0%BE%D0%BB%D0%B0%D1%80%D0%BE%D0%B8%D0%B4.jpg"
      },
      {
        name: "Кирилл Кто",
        id: "original fake",
        url:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Kirill_KTO.JPG/225px-Kirill_KTO.JPG"
      }
    ]
  });
});

module.exports = router;
